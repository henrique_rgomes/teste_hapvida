import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Rotas } from './core/enums/rotas.enum';

const routes: Routes = [
  {
    path: 'consulta-veterinaria',
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '../app/pages/consulta-veterinaria/consulta-veterinaria-listagem/consulta-veterinaria-listagem.module'
          ).then((m) => m.ConsultaVeterinariaListagemModule)
      },
      {
        path: 'formulario',
        loadChildren: () => 
          import(
            '../app/pages/consulta-veterinaria/consulta-veterinaria-formulario/consulta-veterinaria-formulario.module'
          ).then((m) => m.ConsultaVeterinariaFormularioModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: Rotas.CONSULTAVETERINARIA.listagem,
    pathMatch: 'full',
  },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
