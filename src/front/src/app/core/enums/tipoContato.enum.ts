const listaEnumTipoContato: Array<EnumTipoContato> = [];

export class EnumTipoContato {
    static TELEFONE = new EnumTipoContato(1, 'Telefone');
    static EMAIL = new EnumTipoContato(2, 'E-mail');
    
    constructor(public id: number, public descricao: string) {
        listaEnumTipoContato.push(this);
    }
    
    public static values(): Array<EnumTipoContato> {
        return listaEnumTipoContato;
    }
    
    public static getById(id: number): EnumTipoContato {
        return listaEnumTipoContato.filter((tipo) => tipo.id === id)[0];
    }
}