const listaEnumEspecie: Array<EnumEspecie> = []

export class EnumEspecie {
    
    static PEIXE = new EnumEspecie(1, 'Peixe');
    static REPTEIS = new EnumEspecie(2, 'Répteis');
    static AVES = new EnumEspecie(3, 'Aves');
    static MAMIFEROS = new EnumEspecie(4, 'Mamíferos');
    static ANFIBIOS = new EnumEspecie(4, 'Anfíbios');

    constructor(public id: number, public descricao: string) {
        listaEnumEspecie.push(this);
    }
    
    public static values(): Array<EnumEspecie> {
        return listaEnumEspecie;
    }
    
    public static getById(id: number): EnumEspecie {
        return listaEnumEspecie.filter((tipo) => tipo.id === id)[0];
    }
}