const listaEnumStatusConsulta: Array<EnumStatusConsulta> = [];

export class EnumStatusConsulta {
    static ATIVO = new EnumStatusConsulta(1, 'Ativo');
    static CANCELADO = new EnumStatusConsulta(2, 'Cancelado');

    constructor(public id: number, public descricao: string) {
        listaEnumStatusConsulta.push(this);
    }
    
    public static values(): Array<EnumStatusConsulta> {
        return listaEnumStatusConsulta;
    }
    
    public static getById(id: number): EnumStatusConsulta {
        return listaEnumStatusConsulta.filter((tipo) => tipo.id === id)[0];
    }
}