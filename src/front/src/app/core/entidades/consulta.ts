import { Veterinario, Animal } from ".";
import { EnumStatusConsulta } from "../enums";

export class Consulta {
    id: number;
    dataConsulta: Date;
    status: EnumStatusConsulta;
    veterinario: Veterinario;
    animal: Animal;

    constructor(init?: Partial<Consulta>) {
        Object.assign(this, null || init);
    }
}