import { EnumTipoContato } from '../enums';

export class Contato {
    id: number;
    descricao: string;
    tipoContato: EnumTipoContato;

    constructor(init?: Partial<Contato>) {
        Object.assign(this, null || init);
    }
}