import { Contato } from '.';

export class Veterinario {
    id: number;
    nome: string;
    contato: Contato[];

    constructor(init?: Partial<Veterinario>) {
        Object.assign(this, null || init);
    }
}