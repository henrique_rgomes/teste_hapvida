import { EnumEspecie } from '../enums';
import { Tutor } from '.';

export class Animal {
    id: number;
    nome: string;
    especie: EnumEspecie;
    raca: string;
    dataNascimento: Date;
    tutor: Tutor

    constructor(init?: Partial<Animal>) {
        Object.assign(this, null || init);
    }
}