import { Contato } from '.';

export class Tutor {
    id: number;
    nome: string;
    contato: Contato[];

    constructor(init?: Partial<Tutor>) {
        Object.assign(this, null || init);
    }
}