import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Rotas } from '../../../core/enums/rotas.enum';
import { ConsultaVeterinariaListagemComponent } from './consulta-veterinaria-listagem.component';

const routes: Routes = [
  {
    path: '',
    data: {
      rotaPrincipal: Rotas.CONSULTAVETERINARIA.listagem
    },
    component: ConsultaVeterinariaListagemComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})

export class ConsultaVeterinariaListagemRoutingModule { }
