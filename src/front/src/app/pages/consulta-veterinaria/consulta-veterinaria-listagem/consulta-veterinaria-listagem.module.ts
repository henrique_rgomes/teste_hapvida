import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaVeterinariaListagemComponent } from './consulta-veterinaria-listagem.component';
import { ConsultaVeterinariaListagemRoutingModule } from './consulta-veterinaria-listagem-routing.module';

@NgModule({
  declarations: [ConsultaVeterinariaListagemComponent],
  imports: [
    CommonModule,
    ConsultaVeterinariaListagemRoutingModule
  ]
})
export class ConsultaVeterinariaListagemModule { }
