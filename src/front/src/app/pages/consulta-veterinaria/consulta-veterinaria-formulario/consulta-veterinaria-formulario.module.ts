import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultaVeterinariaFormularioComponent } from './consulta-veterinaria-formulario.component';
import { ConsultaVeterinariaFormularioRoutingModule } from './consulta-veterinaria-formulario-routing.module';

@NgModule({
  declarations: [ConsultaVeterinariaFormularioComponent],
  imports: [
    CommonModule,
    ConsultaVeterinariaFormularioRoutingModule
  ]
})
export class ConsultaVeterinariaFormularioModule { }
