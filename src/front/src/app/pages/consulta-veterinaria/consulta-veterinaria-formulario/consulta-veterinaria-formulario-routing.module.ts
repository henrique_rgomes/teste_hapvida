import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Rotas } from '../../../core/enums/rotas.enum';
import { ConsultaVeterinariaFormularioComponent } from './consulta-veterinaria-formulario.component';

const routes: Routes = [
  {
    path: '',
    data: {
      rotaPrincipal: Rotas.CONSULTAVETERINARIA.formulario
    },
    component: ConsultaVeterinariaFormularioComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ConsultaVeterinariaFormularioRoutingModule { }
